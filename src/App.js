import React, { useState } from "react";

require("dotenv").config();

const App = () => {
  const [count, setcount] = useState(0);
  return <div onClick={() => setcount(count + 1)}>empty div {count}</div>;
};

export default App;
